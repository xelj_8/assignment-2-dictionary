%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUFFER_SIZE 256
%define PRINT 1
%define STDERR 2

section .bss
buffer resb BUFFER_SIZE

section .rodata
KEY_NOT_FOUND: db "Key not found", 0
KEY_TOO_LONG: db "Key is too long(>255)", 0

section .text

global _start

_start:
	mov rdi, buffer
        mov rsi, BUFFER_SIZE
        call read_word
        test rax, rax
        jz .ktl

        mov rdi, rax
        mov rsi, END_POINTER_DICT
        push rdx
        call find_word
        pop rdx
        test rax, rax
        jz .knf

        lea rdi, [rax+8+rdx+1] ;предыдущая запись (+8) и ключ-строка (+rdx+1)
	call print_string
	xor rdi, rdi
	call exit

	.ktl:
		mov rdi, KEY_TOO_LONG
		call print_error
		mov rdi, 1
		call exit
	
	.knf:
		mov rdi, KEY_NOT_FOUND
		call print_error
		mov rdi, 1
		call exit
	
print_error:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rax, PRINT
	mov rdi, STDERR
	syscall
	ret
