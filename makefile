ASM=nasm -f elf64
OBJ_FLAG=-o
LD=ld

%.o: %.asm $(wildcard *.inc)
        $(ASM) $(OBJ_FLAG) $@ $<

programm: main.o lib.o dict.o
        $(LD) $(OBJ_FLAG) $@ $^

MESSAGES := "sugar" "" "first" "third" "agsfdghasfdghasfdghasfdghasfdghsafdghasfdghasfdhgasfdghasfdhgsdagfhasgdhgfasdgfasdhfjgdsajfsdghghsfgjhsafghsghdsghsdfaghfagahfghajfgjhvsgcvgdsfcvhgsdfsgdcvdsgfcgsdfcsdvcgsdfcghdsfcdsfghfsagfdghfshgjdfghsdfhgsfdghsghsfdghfsghfghsdfgsdfgdfdgsdfdghsfdhgfsgha" "agsfdghasfdghasfdghasfdghasfdghsafdghasfdghasfdhgasfdghasfdhgsdagfhasgdhgfasdgfasdhfjgdsajfsdghghsfgjhsafghsghdsghsdfaghfagahfghajfgjhvsgcvgdsfcvhgsdfsgdcvdsgfcgsdfcsdvcgsdfcghdsfcdsfghfsagfdghfshgjdfghsdfhgsfdghsghsfdghfsghfghsdfgsdfgdfdgsdfdghsfdhgfsghaa"

test:
        @i=1; \
        for message in $(MESSAGES); do \
                result=$$(echo $$message | ./programm 2>&1); \
                echo "Test $$i: message: $$message result: $$result"; \
                i=$$((i+1)); \
        done


.PHONY: clean programm test
clean:
        rm -f *.o programm
